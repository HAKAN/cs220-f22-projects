# Project 4 (P4): Pokémon Battle Simulation

## Clarifications/Corrections:
* 9/28/2022 10:30 AM - `p4.ipynb` updated - image size reduced. **Please redownload the notebook, if you downloaded the notebook before this update**. Otherwise, Gradescope will **not** display your code while grading.
* 9/28/2022 6:50 PM - **Clarification:** Every Pokémon will have at least one type. Your `get_num_types` and `effective_damage` functions need not handle the case where a Pokémon has zero types.


**Find any issues?** Report to us:

- Sai Raghava Mukund Bhamidipati <bhamidipati3@wisc.edu>
- Jodi Lawson <jlawson6@wisc.edu>

## Instructions:

In this project, we will focus on conditional statements. To start, create a `p4` directory, and download `p4.ipynb`, `project.py`, `test.py`, `pokemon_stats.csv`, and `type_effectiveness_stats.csv`. For each project, we will have you download a new `test.py` - please make sure to download the files for the current project.

**Note:** Please go through [lab-p4](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/lab-p4) before you start the project. The lab contains some very important information that will be necessary for you to finish the project.

You will work on `p4.ipynb` and hand it in. You should follow the provided directions for each question. Questions have **specific** directions on what **to do** and what **not to do**. 

After you've downloaded the file to your `p4` directory, open a terminal window and use `cd` to navigate to that directory. To make sure you're in the correct directory in the terminal, type `pwd`. To make sure you've downloaded the notebook file, type `ls` to ensure that `p4.ipynb`, `project.py`, `test.py`, `pokemon_stats.csv`, and `type_effectiveness_stats.csv` are listed. Then run the command `jupyter notebook` to start Jupyter, and get started on the project!

**IMPORTANT**: You should **NOT** terminate/close the session where you run the above command. If you need to use any other Terminal/PowerShell commands, open a new window instead. Keep constantly saving your notebook file, by either clicking the "Save and Checkpoint" button (floppy disk) or using the appropriate keyboard shortcut.

------------------------------

## IMPORTANT Submission instructions:
- Review the [Grading Rubric](https://git.doit.wisc.edu/cdis/cs/courses/cs220/cs220-f22-projects/-/tree/main/p4/rubric.md), to ensure that you don't lose points during code review.
- You must **save your notebook file** before you run the cell containing **export**.
- Login to [Gradescope](https://www.gradescope.com/) and upload the zip file into the P4 assignment.

